/*
 * sch_dtht: Deep Thought Scheduler
 * Copyright (C) 2011 Daniel Borkmann <borkmann@iogearbox.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * A simple, stupid drop, dup, corrupt, hi-jitter and just for fun 
 * network scheduler! Probably he won't give you the answer to the
 * ultimate question of life, the universe, and everything, but he will
 * take his time thinking about it. ;-)
 *
 *  enable on egress: tc qdisc add dev eth10 root dtht
 * disable on egress: tc qdisc del dev eth10 root dtht
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/skbuff.h>
#include <linux/net.h>
#include <net/pkt_sched.h>
#include <net/sch_generic.h>

#define MODULE_NAME "sch_dtht"
#define MODULE_DESC "Deep Thought Scheduler"

struct sock_time_inf {
	psched_time_t depart;
};

#define SKB_DT_INF(skb) ((struct sock_time_inf *) (qdisc_skb_cb(skb)->data))

static int deepthought_enqueue(struct sk_buff *skb, struct Qdisc *sch)
{
	int drop = 0, dup = 0, corr = 0;
	struct sk_buff *skb2 = NULL;

	if (likely(sch->qstats.backlog + qdisc_pkt_len(skb) <= sch->limit)) {
		if ((net_random() & 0x7) == 1)
			drop = 1;
		if ((net_random() & 0xf) == 1)
			corr = 1;
		if ((net_random() & 0xf) == 1 &&
		    sch->qstats.backlog + qdisc_pkt_len(skb) * 2 <=
		    sch->limit)
			dup = 1;
	} else {
		drop = 1;
	}
	if (drop) {
		kfree_skb(skb);
drop_nofree:
		sch->qstats.drops++;
		return NET_XMIT_DROP;
	}
	if (dup) {
		skb2 = skb_clone(skb, GFP_ATOMIC);
		SKB_DT_INF(skb2)->depart = psched_get_time() +
				 	   (net_random() & 0xffff);
		qdisc_enqueue_tail(skb2, sch);
	}
	if (corr) {
		if (!(skb = skb_unshare(skb, GFP_ATOMIC)) ||
		    (skb->ip_summed == CHECKSUM_PARTIAL &&
		     skb_checksum_help(skb)))
			goto drop_nofree;
		skb->data[net_random() % skb_headlen(skb)] ^= 1 << (net_random() % 8);
	}
	SKB_DT_INF(skb)->depart = psched_get_time() + (net_random() & 0xffff);
	return qdisc_enqueue_tail(skb, sch);
}

static struct sk_buff *deepthought_dequeue(struct Qdisc *sch)
{
	struct sk_buff *skb = qdisc_dequeue_head(sch);
	if (skb) {
		if (psched_get_time() >= SKB_DT_INF(skb)->depart)
			return skb;
		else
			qdisc_enqueue_tail(skb, sch);
	}
	return NULL;
}

static int deepthought_qinit(struct Qdisc *sch, struct nlattr *opt)
{
	u32 limit = qdisc_dev(sch)->tx_queue_len ? : 1;
	limit *= psched_mtu(qdisc_dev(sch));
	sch->limit = limit;
	return 0;
}

static int deepthought_dump(struct Qdisc *sch, struct sk_buff *skb)
{
	struct tc_fifo_qopt opt = {
		.limit = sch->limit
	};
	NLA_PUT(skb, TCA_OPTIONS, sizeof(opt), &opt);
	return skb->len;
nla_put_failure:
	return -1;
}

static struct Qdisc_ops deepthought_qdisc_ops __read_mostly = {
	.id		= "dtht",
	.priv_size	= 0,
	.enqueue	= deepthought_enqueue,
	.dequeue	= deepthought_dequeue,
	.peek		= qdisc_peek_head,
	.drop		= qdisc_queue_drop,
	.init		= deepthought_qinit,
	.reset		= qdisc_reset_queue,
	.change		= deepthought_qinit,
	.dump		= deepthought_dump,
	.owner		= THIS_MODULE,
};

static int __init deepthought_init(void)
{
	printk(KERN_INFO "%s: %s loaded!\n", MODULE_NAME, MODULE_DESC);
	return register_qdisc(&deepthought_qdisc_ops);
}

static void __exit deepthought_exit(void)
{
	unregister_qdisc(&deepthought_qdisc_ops);
	printk(KERN_INFO "%s: removed!\n", MODULE_NAME);
}

module_init(deepthought_init);
module_exit(deepthought_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION(MODULE_DESC);
MODULE_AUTHOR("Daniel Borkmann <borkmann@iogearbox.net>");

